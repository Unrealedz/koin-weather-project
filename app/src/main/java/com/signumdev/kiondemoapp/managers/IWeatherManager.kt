package com.signumdev.kiondemoapp.managers

import com.signumdev.kiondemoapp.model.FiveDaysWeatherResponse


interface IWeatherManager {
    suspend fun getFiveDayWeather(cityName: String): FiveDaysWeatherResponse
}