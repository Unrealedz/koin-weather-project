package com.signumdev.kiondemoapp.managers

import com.signumdev.kiondemoapp.model.FiveDaysWeatherResponse
import com.signumdev.kiondemoapp.repositories.IWeatherRepository

class WeatherManager(private val weatherRepository: IWeatherRepository): IWeatherManager {

    override suspend fun getFiveDayWeather(cityName: String): FiveDaysWeatherResponse {
       return  weatherRepository.getFiveDayWeather(cityName)
    }

}

