package com.signumdev.kiondemoapp

import android.app.Application
import com.signumdev.kiondemoapp.di.appModule
import com.signumdev.kiondemoapp.di.managersModule
import com.signumdev.kiondemoapp.di.serviceModule
import com.signumdev.kiondemoapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin(){
            androidContext(this@App)
            // use Android logger - Level.INFO by default
            androidLogger()
            // use properties from assets/koin.properties
            androidFileProperties()
            modules(modules)
        }
    }

    private val modules =
        listOf(appModule, serviceModule, managersModule, viewModelModule)
}