package com.signumdev.kiondemoapp.ui.data

data class WeatherUiData(
    val date: String,
    val temperature: String,
    val pressure: String,
    val humidity: String
)