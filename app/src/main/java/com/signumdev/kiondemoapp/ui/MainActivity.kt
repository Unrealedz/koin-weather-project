package com.signumdev.kiondemoapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.signumdev.kiondemoapp.R
import com.signumdev.kiondemoapp.mvvm.DataStatus
import com.signumdev.kiondemoapp.viewmodel.WeatherListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val viewModel: WeatherListViewModel by inject()
    private val adapter = SimpleAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeView()
        initializeViewModel()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        viewModel.getFiveDaysWeather("Lviv")
    }

    private fun initializeView(){
        recyclerView.adapter = adapter
        val decor = DividerItemDecoration(
            this,
            LinearLayoutManager.HORIZONTAL
        ).apply { setDrawable(ContextCompat.getDrawable(this@MainActivity, R.drawable.divider)!!) }
        recyclerView.addItemDecoration(decor)
    }

    private fun initializeViewModel(){
        viewModel.dataSource.observe(this, Observer {
            when(it.status){
                DataStatus.SUCCESS -> {
                    it.data?.let { it1 -> adapter.replaceAll(it1.toMutableList()) }
                }
                DataStatus.ERROR -> {
                    Toast.makeText(this, "Error: ${it.error?.message}", Toast.LENGTH_SHORT)
                }
            }
        })
    }
}