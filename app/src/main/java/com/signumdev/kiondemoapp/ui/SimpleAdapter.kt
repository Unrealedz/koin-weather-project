package com.signumdev.kiondemoapp.ui

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.signumdev.kiondemoapp.R
import com.signumdev.kiondemoapp.ui.data.WeatherUiData

class SimpleAdapter: RecyclerView.Adapter<SimpleViewHolder>() {
    private var data: MutableList<WeatherUiData> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_item, parent, false)
        return SimpleViewHolder(view)
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        val weatherInfo = data[position]
        val weatherholder = holder as  SimpleViewHolder
        weatherholder.bind(weatherInfo)
    }

    fun replaceAll(data: MutableList<WeatherUiData>){
        this.data = data
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = data.size
}

class SimpleViewHolder constructor(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    private var temperature: TextView = itemView.findViewById(R.id.temperature)
    private var humidity: TextView = itemView.findViewById(R.id.humidityValue)
    private var pressure: TextView = itemView.findViewById(R.id.pressureValue)
    private var date: TextView = itemView.findViewById(R.id.date)

    fun bind(data: WeatherUiData){
        date.text = data.date
        temperature.text = data.temperature
        humidity.text = data.humidity
        pressure.text = data.pressure
    }
}