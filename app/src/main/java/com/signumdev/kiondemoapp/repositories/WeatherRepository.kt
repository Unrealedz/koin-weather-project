package com.signumdev.kiondemoapp.repositories

import com.signumdev.kiondemoapp.api.WeatherService
import com.signumdev.kiondemoapp.model.FiveDaysWeatherResponse

class WeatherRepository(private val service: WeatherService, private val apiKey: String): IWeatherRepository {
    override suspend fun getFiveDayWeather(cityName: String) : FiveDaysWeatherResponse {
        return service.getFiveDayWeather(apiKey, cityName)
    }
}

interface IWeatherRepository{
    suspend fun getFiveDayWeather(cityName: String) : FiveDaysWeatherResponse
}
