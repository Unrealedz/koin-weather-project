package com.signumdev.kiondemoapp.mvvm

data class DataWrapper<T>(val data: T?,
                          val error: Throwable?,
                          @DataStatus val status: Int
)
