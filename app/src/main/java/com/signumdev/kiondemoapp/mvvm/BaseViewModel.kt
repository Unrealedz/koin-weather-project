package com.signumdev.kiondemoapp.mvvm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Log

open class BaseViewModel: ViewModel() {

    private val TAG = "Base view model: " + this.javaClass.simpleName

    protected fun <T> onSuccess(liveData: MutableLiveData<DataWrapper<T>>, data: T) {
        liveData.value = DataWrapper(data, null, DataStatus.SUCCESS)
    }

    protected fun <T> onLoading(liveData: MutableLiveData<DataWrapper<T>>, throwable: Throwable) {
        Log.e(TAG, "$throwable")
        liveData.value = DataWrapper(null, throwable, DataStatus.LOADING)
    }

    protected fun <T> onError(liveData: MutableLiveData<DataWrapper<T>>, throwable: Throwable) {
        Log.e(TAG, "$throwable")
        liveData.value = DataWrapper(null, throwable, DataStatus.ERROR)
    }

}