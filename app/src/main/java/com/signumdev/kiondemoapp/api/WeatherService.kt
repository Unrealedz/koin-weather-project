package com.signumdev.kiondemoapp.api

import com.signumdev.kiondemoapp.model.FiveDaysWeatherResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
    @GET("forecast?units=metric")
    suspend fun getFiveDayWeather(@Query("appid") apiKey: String, @Query("q") cityName: String): FiveDaysWeatherResponse
}