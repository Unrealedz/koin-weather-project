package com.signumdev.kiondemoapp.viewmodel

import android.content.Context
import android.text.format.DateUtils
import androidx.lifecycle.*
import com.signumdev.kiondemoapp.managers.IWeatherManager
import com.signumdev.kiondemoapp.mvvm.BaseViewModel
import com.signumdev.kiondemoapp.mvvm.DataWrapper
import com.signumdev.kiondemoapp.repositories.IWeatherRepository
import com.signumdev.kiondemoapp.ui.WeatherUiDataMapper
import com.signumdev.kiondemoapp.ui.data.WeatherUiData
import kotlinx.coroutines.launch

class WeatherListViewModel(private val context: Context, private val manager: IWeatherManager) : BaseViewModel() {

    private val _dataSource = MutableLiveData<DataWrapper<List<WeatherUiData>>>()
    val dataSource: LiveData<DataWrapper<List<WeatherUiData>>>
        get() = _dataSource

    private val mapper: WeatherUiDataMapper by lazy { WeatherUiDataMapper(context) }

    fun getFiveDaysWeather(city: String) {
        viewModelScope.launch {
            val result = manager.getFiveDayWeather(city)
            val uiData = mapper.mapUiData(result)

            onSuccess(_dataSource, uiData)
        }
    }
}