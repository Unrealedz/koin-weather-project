package com.signumdev.kiondemoapp.di

import com.signumdev.kiondemoapp.viewmodel.WeatherListViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { WeatherListViewModel(context = androidContext(), manager = get()) }
}