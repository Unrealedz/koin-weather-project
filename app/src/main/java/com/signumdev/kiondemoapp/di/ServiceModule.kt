package com.signumdev.kiondemoapp.di

import com.signumdev.kiondemoapp.api.WeatherService
import org.koin.dsl.module
import retrofit2.Retrofit

val serviceModule = module {
    single { createService<WeatherService>(retrofit = get()) }
}

private inline fun <reified T> createService(retrofit: Retrofit): T {
    return retrofit.create(T::class.java)
}
