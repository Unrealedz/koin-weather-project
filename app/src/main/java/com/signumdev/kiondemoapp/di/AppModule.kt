package com.signumdev.kiondemoapp.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.signumdev.kiondemoapp.di.Properties.API_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Modifier


object Properties {
    const val API_URL = "API_URL"
}

val appModule = module {
    single { provideGson() }
    single { provideClient() }
    single { provideRetrofit(gson = get(), okHttpClient = get(), baseUrl = getProperty(API_URL)) }
}

private fun provideGson(): Gson {
    return GsonBuilder()
        .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
        .serializeNulls()
        .create()
}

private fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient, baseUrl: String): Retrofit {
    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(okHttpClient)
        .build()
}

fun provideClient(): OkHttpClient {
    val builder = OkHttpClient.Builder()
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY
    builder.addInterceptor(logging)

    return builder.build()
}

