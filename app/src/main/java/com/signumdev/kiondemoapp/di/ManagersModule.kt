package com.signumdev.kiondemoapp.di

import com.signumdev.kiondemoapp.managers.IWeatherManager
import com.signumdev.kiondemoapp.managers.WeatherManager
import com.signumdev.kiondemoapp.repositories.IWeatherRepository
import com.signumdev.kiondemoapp.repositories.WeatherRepository
import org.koin.dsl.module

const val API_KEY = "API_KEY"

val managersModule = module {
    single<IWeatherRepository> { WeatherRepository(service = get(), apiKey = getProperty(API_KEY)) }
    single<IWeatherManager> { WeatherManager(get()) }
}

