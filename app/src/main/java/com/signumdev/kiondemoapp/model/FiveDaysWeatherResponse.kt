package com.signumdev.kiondemoapp.model

import com.google.gson.annotations.SerializedName

data class FiveDaysWeatherResponse(
    @SerializedName("list")
    var listDao: MutableList<WeatherData> = mutableListOf()
)

data class WeatherData(
    @SerializedName("dt")
    var dt: Long,
    @SerializedName("main")
    var main: Main,
    @SerializedName("weathers")
    var weathers: List<Weather> = mutableListOf()
)

data class Main(
    @SerializedName("temp")
    var temp: Double,
    @SerializedName("pressure")
    var pressure: Int,
    @SerializedName("humidity")
    var humidity: Int
)

data class Weather(
    @SerializedName("main")
    var main: String,
    @SerializedName("description")
    var description: String
)